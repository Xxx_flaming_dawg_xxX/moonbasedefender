# Moon Base Defender #

moon base defender is a 2d game made with Godot.

In this game you protect the moon by shooting incoming asteroids and upgrading your weapon

## Download ##
you can download the game from [itch.io](https://roei.itch.io/moon-base-defender) for free.

## How to play ##
w / ^ - jump

a / < - move left

d / > - move right

Aim with the mouse

Shoot with LMB

Blue arrows indicate asteroid locations



## How to compile/run from source ##
* Get Godot from the [official website](https://godotengine.org/) or [compile Godot yourself](http://docs.godotengine.org/en/latest/reference/_compiling.html)
* Clone this repository
* Open Godot, select "import" and choose engine.cfg file from this repository
* Run it from Godot's menu or from the editor.