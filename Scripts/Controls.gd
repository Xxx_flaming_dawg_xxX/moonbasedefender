extends Control

const BG_SPEED = 12.5

func _ready():
	set_process(true)

func _process(delta):
	var stars_pos = get_node("Background/Stars").get_pos()+Vector2(-BG_SPEED*delta,0)
	if(stars_pos.x <= -800*2):
		stars_pos = Vector2(0,0)
	get_node("Background/Stars").set_pos(stars_pos)

func _on_Button_pressed():
	get_tree().change_scene("res://Scenes/MainMenu.tscn")
