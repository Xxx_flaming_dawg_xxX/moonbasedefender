extends Control

func _ready():
	var diff_name
	if(globals.difficulty >= 2):
		diff_name = "Impossible"
	else:
		diff_name = globals.DIFF_NAMES[str(globals.difficulty)]
		
	get_node("Container/Score").set_text("You reached difficulty "+diff_name+" with score " + str(globals.score))
	set_process(true)
	
func _process(delta):
	globals.stars_pos.x -= delta*globals.BG_SPEED
	if(globals.stars_pos.x <= -800*2):
		globals.stars_pos = Vector2(0,0)
	get_node("Background/Stars").set_pos(globals.stars_pos)

func _on_Retry_pressed():
	get_tree().change_scene("res://Scenes/SetDiff.tscn")


func _on_Quit_pressed():
	get_tree().quit()
