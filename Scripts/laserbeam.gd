extends RigidBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
const FIRE_DAMAGE = 1

var ttl = 5
var hits = 2
var fire_speed = 400

func _ready():
	set_process(true)
	set_fixed_process(true)

func _set_hits(value):
	hits = value

func _set_speed(value):
	fire_speed = value

func _process(delta):
	ttl -= delta
	var bodies = get_colliding_bodies()
	
	for body in bodies:
		if !body.is_queued_for_deletion():
			if body.is_in_group("Asteroids"):
				var old_hits = hits
				hits = int((-body.hit(FIRE_DAMAGE*hits))/FIRE_DAMAGE)
				if(hits > 0):
					get_node("../../..")._add_points(old_hits - hits)
				else:
					get_node("../../..")._add_points(old_hits)
			elif body.is_in_group("Walls") || body.is_in_group("Border"):
				hits=0

	if(hits <= 0 || ttl <= 0):
		queue_free()

func _fixed_process(delta):
	set_linear_velocity(Vector2(fire_speed,0).rotated(get_rot()))