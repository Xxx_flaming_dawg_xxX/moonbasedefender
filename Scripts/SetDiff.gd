extends Control

func _ready():
	set_process(true)
	globals.score = 0

func _process(delta):
	globals.stars_pos.x -= delta*globals.BG_SPEED
	if(globals.stars_pos.x <= -800*2):
		globals.stars_pos = Vector2(0,0)
	get_node("Background/Stars").set_pos(globals.stars_pos)

func _on_HSlider_value_changed(value):
	globals.difficulty = value
	get_node("Container/Label").set_text(globals.DIFF_NAMES[str(value)])


func _on_Start_pressed():
	get_tree().change_scene("res://Scenes/Level.tscn")
