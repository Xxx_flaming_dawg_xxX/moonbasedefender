extends KinematicBody2D

signal s_next_level

var player_speed = 230
var velocity = Vector2(0,0)
var frame = 0
var jumping = true

var charge_time = 0.3
var shot_speed = 400

var shot_hits = 1
var recharge = 0

var to_upgrade = -1

func _ready():
	set_fixed_process(true)
	set_process(true)

func _process(delta):
	if is_colliding() && get_collision_normal().y == -1:
		jumping = false
		velocity.y = 0
	else:
		velocity.y += delta*player_speed*globals.GRAVITY
	
	get_node("Sprite/gun").set_rot(get_local_mouse_pos().angle()-PI/2)
	get_node("Sprite/gun").set_flip_v(get_local_mouse_pos().x < 0)
	
	if recharge > 0:
		recharge -= delta
	
	if(Input.is_action_pressed("next_level") && to_upgrade != -1):
		_apply_upgrade()
	
	if(Input.is_mouse_button_pressed(BUTTON_LEFT) && recharge <= 0):
		var laser = load("res://Scenes/laserbeam.tscn").instance()
		get_node("Beams").add_child(laser)
		laser._set_speed(shot_speed)
		laser._set_hits(shot_hits)
		laser.set_pos(Vector2(18,0).rotated(get_node("Sprite/gun").get_rot()))
		laser.set_rot(get_node("Sprite/gun").get_rot())
		laser.add_to_group("beams",true)
		recharge = charge_time
	
	if Input.is_action_pressed("jump") && !jumping:
		velocity.y = -player_speed
		jumping = true
	elif Input.is_action_pressed("move_left"):
		if(velocity.x > 0):
			velocity.x = 0
		velocity.x = max(-player_speed,velocity.x-((player_speed*delta)/globals.GRAVITY))
	elif Input.is_action_pressed("move_right"):
		if(velocity.x < 0):
			velocity.x = 0
		velocity.x = min(player_speed,velocity.x+(player_speed*delta)/globals.GRAVITY)
	else:
		velocity.x = 0
	
	if velocity.x < 0:
		get_node("Sprite").set_flip_h(true)
		get_node("Sprite").set_offset(Vector2(-7,0))
	elif velocity.x > 0:
		get_node("Sprite").set_flip_h(false)
		get_node("Sprite").set_offset(Vector2(0,0))

	if jumping:
		get_node("Sprite").set_frame(6)
	elif(velocity.x != 0):
		frame += globals.RUN_FPS*delta*(abs(velocity.x)/player_speed)
		get_node("Sprite").set_frame(int(frame)%6)
	else:
		get_node("Sprite").set_frame(0)

func _fixed_process(delta):
	if is_colliding():
		var normal = get_collision_normal()
		if abs(normal.x) == 1 || normal.y != -1:
			velocity = normal.slide(velocity)
	move(delta*velocity)

func _apply_upgrade():
	#upgrade charge time
	if(to_upgrade == 0):
		if(charge_time > 0.03):
			charge_time -= 0.02
		else:
			charge_time = 0.01
			get_node("Upgrades/+cspd").set_hidden(true)
	
	#upgrade damage
	elif(to_upgrade == 1):
		shot_hits += 1
	
	#upgrade shot speed
	elif(to_upgrade == 2):
		shot_speed += 20
		if(shot_speed >= 700):
			get_node("Upgrades/+sspd").set_hidden(true)
	
	#upgrade player speed
	elif(to_upgrade == 3):
		player_speed += 10
		if(player_speed >= 350):
			get_node("Upgrades/+pspd").set_hidden(true)
			
	#heal base
	elif(to_upgrade == 4):
		get_parent().hit(-15)
	
	to_upgrade = -1
	get_node("Upgrades").set_hidden(true)
	get_node("Upgrades/Selected").set_hidden(true)
	emit_signal("s_next_level")
	
	
			
func _on_cspd_body_enter( body ):
	if !get_node("Upgrades/+cspd").is_hidden() && !get_node("Upgrades").is_hidden() && body.is_in_group("beams"):
		get_node("Upgrades/Selected").set_pos(get_node("Upgrades/+cspd").get_pos())
		get_node("Upgrades/Selected").set_hidden(false)
		to_upgrade = 0

func _on_dmg_body_enter( body ):
	if !get_node("Upgrades/+dmg").is_hidden() && !get_node("Upgrades").is_hidden() && body.is_in_group("beams"):
		get_node("Upgrades/Selected").set_pos(get_node("Upgrades/+dmg").get_pos())
		get_node("Upgrades/Selected").set_hidden(false)
		to_upgrade = 1

func _on_sspd_body_enter( body ):
	if !get_node("Upgrades/+sspd").is_hidden() && !get_node("Upgrades").is_hidden() && body.is_in_group("beams"):
		get_node("Upgrades/Selected").set_pos(get_node("Upgrades/+sspd").get_pos())
		get_node("Upgrades/Selected").set_hidden(false)
		to_upgrade = 2

func _on_pspd_body_enter( body ):
	if !get_node("Upgrades/+pspd").is_hidden() && !get_node("Upgrades").is_hidden() && body.is_in_group("beams"):
		get_node("Upgrades/Selected").set_pos(get_node("Upgrades/+pspd").get_pos())
		get_node("Upgrades/Selected").set_hidden(false)
		to_upgrade = 3

func _on_hp_body_enter( body ):
	if !get_node("Upgrades/+hp").is_hidden() && !get_node("Upgrades").is_hidden() && body.is_in_group("beams"):
		get_node("Upgrades/Selected").set_pos(get_node("Upgrades/+hp").get_pos())
		get_node("Upgrades/Selected").set_hidden(false)
		to_upgrade = 4
