extends Node
#This script is a singleton that holds all settings for the game

const TO_RAD = (2*PI/360)
const EARTH_SPEED = 360/100
const BG_SPEED = 25

const GRAVITY = 1
const RUN_FPS = 14

const BASE_ASTEROID_SPEED = Vector2(100,0)

#Levels settings
const LEVEL_SIZE = Vector2(1440,592)
const MAX_SIZE = [2.5,2,1,0.7,0.5,0.3,5]
const MAX_SPEED = [1,1.5,1.7,2,2.3,2.7,0.3]
const MAX_HITS = [1,5,7,9,9,7,50]
const DELAY = [3,2.5,2,1.8,1.5,1.3,3]
const ASTEROIDS_COUNT = [1,3,5,8,12,15,1]
const MAX_LEVEL = 7

#Difficulty names
const DIFF_NAMES = {
	'0.2':"I wont judge you",
	'0.4':"Very Easy",
	'0.6':"Easy",
	'0.8':"Slightly Easy",
	'1':"Normal",
	'1.2':"Slightly Hard",
	'1.4':"Hard",
	'1.6':"Harder",
	'1.8':"Very Hard",
	'2':"Yeah good luck with that"}

var stars_pos = Vector2(0,0)

var difficulty = 1
var score = 0