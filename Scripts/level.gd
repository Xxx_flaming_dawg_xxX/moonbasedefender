extends Control

var asteroids_to_go = globals.ASTEROIDS_COUNT[0]
var asteroids_left = 0
var next_asteroid = globals.DELAY[0]

var level = 0
var hp = 100

var to_upgrade = -1

func _ready():
	randomize()
	set_process(true)
	set_process_input(true)
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	get_node("Player").connect("s_next_level",self,"_next_level",[])

func _input(event):
	if(event.type == InputEvent.KEY && event.scancode == KEY_ESCAPE):
		get_node("Pausemenu/Buttons").set_hidden(false)
		get_tree().set_pause(true)

func _process(delta):
	next_asteroid -= delta
	if(next_asteroid <= 0 && asteroids_to_go > 0):
		var asteroid = load("res://Scenes//Asteroid.tscn").instance()
		var arrow = load("res://Scenes//Arrow.tscn").instance()
		
		get_node("Player/Arrows").add_child(arrow)
		get_node("Asteroids").add_child(asteroid)
		
		var x = randi()%int(globals.LEVEL_SIZE.x-asteroid.get_item_rect().size.x)+(asteroid.get_item_rect().size.x/2)
		var rot = randi()%120 + 250
		var speed = rand_range(1,globals.MAX_SPEED[level])*globals.difficulty
		var frame = randi()%5
		var hits = randi()%max(int(globals.MAX_HITS[level]*globals.difficulty),1) + 1

		asteroid._set_hits(hits)
		asteroid.get_node("Sprite").set_frame(frame)
		asteroid.get_node("Sprite").set_rot(rot*globals.TO_RAD)
		asteroid.set_pos(Vector2(1,-1)*Vector2(x,asteroid.get_item_rect().size.y))
		asteroid.set_linear_velocity((globals.BASE_ASTEROID_SPEED*speed).rotated(rot*globals.TO_RAD))
		asteroid.connect("s_destroyed",self,"_on_asteroid_destroyed",[])
		
		next_asteroid += globals.DELAY[level]
		asteroids_to_go -= 1
		asteroids_left += 1
	elif(asteroids_left == 0 && next_asteroid <= 0):
		get_node("Player/Upgrades").set_hidden(false)
	_arrange_arrows()
	
	var bg = get_node("Background/Tex")
	bg.set_rot(bg.get_rot()+delta*(globals.TO_RAD*globals.EARTH_SPEED))
	
	globals.stars_pos.x -= delta*globals.BG_SPEED
	if(globals.stars_pos.x <= -800*2):
		globals.stars_pos = Vector2(0,0)
	elif(globals.stars_pos.x > 0):
		globals.stars_pos = Vector2(-800*2,0)
	get_node("Background/Stars").set_pos(globals.stars_pos)

func _arrange_arrows():
	var arrows = get_node("Player/Arrows").get_children()
	var asteroids = get_node("Asteroids").get_children()
	for i in range(asteroids.size()):
		arrows[i].set_rotation(get_node("Player").get_global_pos().angle_to_point(asteroids[i].get_global_pos())+PI/2)

func _on_asteroid_destroyed():
	var arrows = get_node("Player/Arrows").get_children()
	if(!arrows.empty()):
		arrows[-1].queue_free()
		arrows.pop_back()
	asteroids_left -= 1
	
func _next_level():
	var arrows = get_node("Player/Arrows").get_children()
	level = (level+1)%globals.MAX_LEVEL
	if(level == 0):
		globals.difficulty += 0.2
	while(!arrows.empty()):
		arrows.pop_back()
	asteroids_to_go = globals.ASTEROIDS_COUNT[level]
	next_asteroid = globals.DELAY[level]

func _add_points(value):
	globals.score += value
	get_node("Background/Score").set_text("Score: " + str(globals.score))

func hit(value):
	hp -= value
	if(hp > 0):
		get_node("Player/Upgrades/+hp").set_hidden(hp > 95)
		get_node("Background/BaseHealth/ProgressBar").set_value(hp)
		return hp
	else:
		get_tree().change_scene("res://Scenes/GameOver.tscn")
		return 0