extends Control

func _ready():
	set_process(true)

func _process(delta):
	var earth = get_node("Background/Earth")
	earth.set_rot(earth.get_rot()+delta*(globals.TO_RAD*globals.EARTH_SPEED))

	globals.stars_pos.x -= delta*globals.BG_SPEED
	if(globals.stars_pos.x <= -800*2):
		globals.stars_pos = Vector2(0,0)
	get_node("Background/Stars").set_pos(globals.stars_pos)
	
	
func _on_Button_pressed():
	get_tree().change_scene("res://Scenes/SetDiff.tscn")

func _on_Button1_pressed():
	get_tree().quit()


func _on_Controls_pressed():
	get_tree().change_scene("res://Scenes/Controls.tscn")
