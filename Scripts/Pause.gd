extends CanvasLayer

func _ready():
	pass

func _on_Resume_pressed():
	get_node("Buttons").set_hidden(true)
	get_tree().set_pause(false)


func _on_MainMenu_pressed():
	get_tree().set_pause(false)
	get_tree().change_scene("res://Scenes/MainMenu.tscn")
