extends RigidBody2D

signal s_destroyed

var hits = 20

func _ready():
	set_process(true)

func _process(delta):
	var bodies = get_colliding_bodies()
	
	for body in bodies:
		if hits > 0 && body.is_in_group("Walls") || body.is_in_group("Player"):
			get_parent().get_parent().hit(hits)
			hit(hits)
	
func _set_hits(value):
	hits = value
	get_node("hp").set_max(hits)
	get_node("hp").set_value(hits)

func hit(dmg):
	hits -= dmg
	get_node("hp").set_value(hits)
	if(hits <= 0):
		queue_free()
		emit_signal("s_destroyed")
		
	return hits